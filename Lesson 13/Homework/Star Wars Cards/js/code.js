let charactersArray = [];
let savedCharacters = [];
let cardMinWidth = 200;
let fontSize = 16;
let textWidth = 1;
let buttonWidth = 200;
let fontSizeIncrement = 0.4;
let textWidthIncrement = 0.03;
let buttonWidthIncrement = 20;

class Character {
	constructor(name, height, mass, hair_color, skin_color) {
		this.name = name;
		this.height = height;
		this.mass = mass;
		this.hair_color = hair_color;
		this.skin_color = skin_color;
	};
	showCharacter() {
		let card = document.createElement("div");
		card.classList.add("card");
		card.style.minWidth = `${cardMinWidth}px`
		cardMinWidth += 50;

		let nameDiv = document.createElement("div");
		nameDiv.style.fontSize = `${fontSize}px`;
		fontSize += fontSizeIncrement;
		nameDiv.style.transform = `scale(${textWidth}, 1)`;
		textWidth += textWidthIncrement;

		let heightDiv = document.createElement("div");
		heightDiv.style.fontSize = `${fontSize}px`;
		fontSize += fontSizeIncrement;
		heightDiv.style.transform = `scale(${textWidth}, 1)`;
		textWidth += textWidthIncrement;

		let massDiv = document.createElement("div");
		massDiv.style.fontSize = `${fontSize}px`;
		fontSize += fontSizeIncrement;
		massDiv.style.transform = `scale(${textWidth}, 1)`;
		textWidth += textWidthIncrement;

		let hair_colorDiv = document.createElement("div");
		hair_colorDiv.style.fontSize = `${fontSize}px`;
		fontSize += fontSizeIncrement;
		hair_colorDiv.style.transform = `scale(${textWidth}, 1)`;
		textWidth += textWidthIncrement;

		let skin_colorDiv = document.createElement("div");
		skin_colorDiv.style.fontSize = `${fontSize}px`;
		fontSize += fontSizeIncrement;
		skin_colorDiv.style.transform = `scale(${textWidth}, 1)`;
		textWidth += textWidthIncrement;

		let saveButton = document.createElement("button");
		saveButton.classList.add("savebutton");
		saveButton.style.width = `${buttonWidth}px`;
		buttonWidth += buttonWidthIncrement;
		saveButton.innerText = "SAVE";
		saveButton.style.fontFamily = `'Star Jedi', arial`;

		saveButton.addEventListener("click", () => {
			charactersArray.forEach(element => {
				if (saveButton.parentNode.firstElementChild.innerText.includes(element.name)) {
					savedCharacters.push(element);
					localStorage.setItem('savedCharacters', JSON.stringify(savedCharacters));
					saveButton.innerText = `SAVED`;
					saveButton.style.color = "black";
					saveButton.style.fontSize = "20px";
					saveButton.style.webkitTextStroke = "1px #FFD94C"
					saveButton.parentNode.style.color = "black";
					saveButton.parentNode.style.webkitTextStroke = "1px #FFD94C"

				}

			});
		});

		nameDiv.innerHTML = `<b>Name:</b> ${this.name}`;
		heightDiv.innerHTML = `<b>Height:</b> ${this.height}cm`;
		massDiv.innerHTML = `<b>Mass:</b> ${this.mass}kg`;
		hair_colorDiv.innerHTML = `<b>Hair color:</b> ${this.hair_color}`;
		skin_colorDiv.innerHTML = `<b>Skin color:</b> ${this.skin_color}`;

		card.append(nameDiv, heightDiv, massDiv, hair_colorDiv, skin_colorDiv, saveButton);
		document.body.append(card);

	};
};

let url = "https://swapi.py4e.com/api/people";

fetch(url)
	.then((r) => r.json())
	.then((r) => {
		r.results.forEach(element => {
			let char = new Character(element.name, element.height, element.mass, element.hair_color, element.skin_color);
			char.showCharacter();
			charactersArray.push(char);
		});
	});

$(document).ready(function () {
	$("body").ready(function () {
		$('body').on('click', function () {
			$('body').off('click');
		});
		$("html, body").animate({
			scrollTop: $(
				'html, body').get(0).scrollHeight
		}, 25000);

	});
});