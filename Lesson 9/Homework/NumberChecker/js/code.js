let container = document.getElementById("container");
let input = document.createElement("input");
container.appendChild(input);

let btn = document.createElement("button");
btn.innerText = "Зберегти";
input.after(btn);

let pattern = /^\d\d\d-\d\d\d-\d\d-\d\d$/;
let messageExists = false;
let errorMessage = document.createElement("div");
errorMessage.setAttribute("class", "error")
errorMessage.innerText = 'Номер введений в неправильному форматі. Будь ласка, перевірте формат і натисніть "Зберегти" ще раз.';

const errorAnimation = () => {
	document.querySelector(".error").remove();
	setTimeout(() => { container.prepend(errorMessage) }, "200");
}

const error = () => {

	if (messageExists) {
		errorAnimation();
		return;
	}
	container.prepend(errorMessage);
	messageExists = true;

}

const check = () => {
	let number = document.querySelector("input").value.trim();


	if (pattern.test(number)) {
		document.querySelector("body").style.backgroundColor = "green";
		if (messageExists) {
			document.querySelector(".error").remove();

		}
		document.location.href = "https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg";
	} else {
		return error();
	}

}


btn.addEventListener("click", check);