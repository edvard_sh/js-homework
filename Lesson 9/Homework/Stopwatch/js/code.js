
let milliseconds = 0, seconds = 0, minutes = 0, intervalHandler, intervalRunning;

const count = () =>{
	milliseconds += 10;
	if(milliseconds === 1000){
		milliseconds = 0;
		
		seconds++;
	}

	if(milliseconds>99){
		document.querySelector("#stopwatchms").textContent = milliseconds/10;

	}

	if(seconds<10){
		document.querySelector("#stopwatchs").textContent = "0" + seconds;
	}else{
		document.querySelector("#stopwatchs").textContent = seconds;

	}
	if(seconds === 60){
		seconds = 0;
		minutes++;
	}
	if(minutes<10){
		document.querySelector("#stopwatchm").textContent = "0" + minutes;
	}else{
		document.querySelector("#stopwatchm").textContent = minutes;

	}
}
let bg = document.querySelector(".stopwatch-display");

console.log(bg);

const start = () => {
	intervalHandler = setInterval(count, 10);
	bg.classList.add("green");
	intervalRunning = true;
}

const stop = () => {
	clearInterval(intervalHandler);
	bg.classList.remove("green");
	intervalRunning = false;

	bg.classList.add("red");

}
const reset = () => {
	milliseconds = 0, seconds = 0, minutes = 0;
	document.querySelector("#stopwatchms").textContent = "00";
	document.querySelector("#stopwatchs").textContent = "00";
	document.querySelector("#stopwatchm").textContent = "00";
	if(!intervalRunning){
		bg.classList.remove("green");
		bg.classList.remove("red");
		bg.classList.add("silver");

	}
}

document.querySelector("#startButton").addEventListener("click", start);
document.querySelector("#stopButton").addEventListener("click", stop);
document.querySelector("#resetButton").addEventListener("click", reset);

