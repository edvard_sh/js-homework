let container = document.getElementById("container");
let [...images] = document.querySelectorAll("img");
let intervalHandler;
let c = 0;
let rightToLeft = true;

for (let i = 0; i < images.length; i++) {
	if (i != 0) {
		images[i].style.left = "800px";
	} else {
		images[i].style.left = "0px";

	}
}

const moveSlides = () => {
	if (rightToLeft) {
		if (c < images.length) {
			images[c].style.left = `${parseInt(images[c].style.left) - 800}px`;
			if (images[c + 1]) {
				images[c + 1].style.left = `${parseInt(images[c + 1].style.left) - 800}px`;
			}
			c++;
			if (c === images.length) {
				rightToLeft = false;
			}
		}

	}
	if (!rightToLeft) {
		if (c > 0) {
			if (images[c]) {
				images[c].style.left = `${parseInt(images[c].style.left) + 800}px`;

			}
			images[c - 1].style.left = `${parseInt(images[c - 1].style.left) + 800}px`;
			c--;
			if (c === 0) {
				rightToLeft = true;

			}
		}
	}
}

window.addEventListener("load", () => {
	intervalHandler = setInterval(moveSlides, 3000);
});
