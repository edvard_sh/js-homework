function Human(age) {
	this.age = age;
	
	this.createHumanArray = function(amount){
		let arr = [];
		for(let i = 0; i<amount; i++){
			arr[i] = new Human(Math.floor(Math.random()*100));
		}
		return arr;
	}
	this.displayHumanArray = function(arr){
		for(let i = 0; i<arr.length; i++){
			document.getElementById("arrayDisplay").innerHTML += `${arr[i].age}</br>`;
		}

	}
	this.sortHumanArray = function(arr){
		for(let i = 0; i<arr.length; i++){
		
			for(j=1; j<arr.length-i; j++){
				if(arr[j-1].age>arr[j].age){
					[arr[j-1], arr[j]] = [arr[j], arr[j-1]];
				}
			}
		}
	}
}

let human = new Human();
const humanArray = human.createHumanArray(5);
human.displayHumanArray(humanArray);
human.sortHumanArray(humanArray);
document.getElementById("arrayDisplay").innerHTML += ` </br>`;
human.displayHumanArray(humanArray);
