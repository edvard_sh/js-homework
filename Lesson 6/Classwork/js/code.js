function Calculator(num1, num2){
this.num1 = num1;
this.num2 = num2;

	this.read = function() {
		this.num1 = parseFloat(prompt("Enter first number:")) ;
		this.num2 = parseFloat(prompt("Enter second number:")) ;
		alert("Thank you, your input was recorded.")
	}

	this.sum = function() {
		return parseFloat(this.num1) + parseFloat(this.num2);
	}

	this.mul = function() {
		return parseFloat(this.num1) * parseFloat(this.num2);
	}
}
var calc = new Calculator();
calc.read();
var sum = calc.sum();
var mul = calc.mul();
document.getElementById("calculator").innerHTML = `The sum of these two numbers equals ${sum}. </br>` ;
document.getElementById("calculator").innerHTML +=  `The multiplication of these two numbers equals ${mul}. </br>` ;