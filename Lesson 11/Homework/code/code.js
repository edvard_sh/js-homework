//Создай класс, который будет создавать пользователей с именем и фамилией. Добавить к классу метод для вывода имени и фамилии 
class User {
	constructor(name, lastName) {
		this.name = name;
		this.lastName = lastName;
	}
	show = () => {
		document.querySelector("body").prepend(`The user's name is ${this.name} and the user's last name is ${this.lastName}`);
	}
}
let user = new User("Edvard", "Shushkovskyi");
user.show();

//Создай список состоящий из 4 листов. Используя джс обратись к 2 li и с использованием навигации по DOM дай 1 элементу синий фон, а 3 красный
let li2 = document.querySelectorAll("li")[1];
li2.previousElementSibling.style.backgroundColor = "blue";
li2.nextElementSibling.style.backgroundColor = "red";

//Создай див высотой 200 пикселей и добавь на него событие наведения мышки. При наведении мышки выведите текстом координаты, где находится курсор мышки
let hoverDiv = document.querySelector('#hoverdiv');
hoverDiv.addEventListener("mousemove", (e) => {
	hoverDiv.innerHTML = `X: ${e.clientX} </br> Y: ${e.clientY}`;
})

//Создай кнопки 1,2,3,4 и при нажатии на кнопку выведи информацию о том какая кнопка была нажата
let btnContainer = document.querySelector('#btncontainer');
btnContainer.addEventListener("click", (e) => {
	btnContainer.lastElementChild.innerHTML = `You clicked on ${e.target.innerHTML}.`
})

//Создай див и сделай так, чтобы при наведении на див, див изменял свое положение на странице
let movingDiv = document.querySelector('#movingdiv');
movingDiv.addEventListener("mouseover", () => {
	movingDiv.style.left = `${Math.random() * 1720}px`;
	movingDiv.style.top = `${Math.random() * 680}px`;
	console.log(movingDiv.style.top);
	console.log(movingDiv.style.left);
});

//Создай поле для ввода цвета, когда пользователь выберет какой-то цвет сделай его фоном body
let colorInput = document.querySelector('input[type="color"]');
colorInput.addEventListener("input", () => {
	document.body.style.backgroundColor = colorInput.value;
});

//Создай инпут для ввода логина, когда пользователь начнет Вводить данные в инпут выводи их в консоль
let loginInput = document.querySelector('#logininput');
loginInput.addEventListener("input", () => {
	console.log(loginInput.value);
})

//Создайте поле для ввода данных поcле введения данных выведите текст под полем 
let dataInput = document.querySelector('#datainput');
let span = document.createElement("span");
dataInput.after(span);
dataInput.addEventListener("change", () => {
	span.innerText = dataInput.value;
});

