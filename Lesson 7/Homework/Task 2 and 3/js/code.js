function User(firstname, lastname, birthday) {
  this.firstName = firstname;
  this.lastName = lastname;
	this.birthday = birthday;

	this.getLogin = function(){
		let newLogin = (this.firstName.charAt(0) + this.lastName).toLowerCase();
		return newLogin;
	}
	this.getAge = function(){
		let arr = this.birthday.split(".");
		let birthdayDate = new Date(`${arr[2]}-${arr[1]}-${arr[0]}`);
		let now = new Date();
		let age = Math.floor((now.getTime() - birthdayDate.getTime())/(1000*60*60*24*365));
		return age;
	}
	this.getPassword = function(){
		let arr = this.birthday.split(".");
		let password = this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + arr[2];
		return password;
	}
}
function createNewUser(firstname, lastname, birthday){
	let newUser = new User(firstname, lastname, birthday);
	return newUser;
}

let user = createNewUser(prompt("Введіть ім'я"), prompt("Введіть прізвище"), prompt("Введіть дату народження в форматі дд.мм.рррр"));
console.log(user);
console.log("Ваш логін: " + user.getLogin());
console.log("Ваш вік: " + user.getAge());
console.log("Ваш пароль: " + user.getPassword());