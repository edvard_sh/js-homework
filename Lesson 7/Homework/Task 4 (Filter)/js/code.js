function filterBy(arr, varType){
let resArray = [];
let counter = 0;

for(element of arr){
	if(element === null){
		if(varType==='null'){
			continue;
		}
		resArray[counter] = element;
		counter++;	
		continue;
	}
	if(typeof(element) !== varType){
	resArray[counter] = element;
	counter++;
	}
};
return resArray;
}

let array = ['hello', 'world', 23, '23', null];
console.log(filterBy(array, 'null'));