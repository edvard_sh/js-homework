const [...allButtons] = document.querySelectorAll(".btn");

document.addEventListener("keypress", (event) => {
	let input = event.key;
	if (input.length <= 1) {
		input = input.toUpperCase();
	}
	allButtons.forEach(element => {
		if (input === element.innerHTML) {
			if (element.classList.contains("active")) {
				element.classList.remove("active");
			} else {
				element.classList.add("active");
			}
		}
	});
})