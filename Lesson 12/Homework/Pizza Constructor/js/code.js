let saucesPrice = 0;
let toppingsPrice = 0;
let sizePrice = 250;
let totalPrice = 0;
let PriceSpanExists = false;
let priceSpan = document.createElement("span");

let [...spans] = document.querySelectorAll("span");

const showTotalPrice = () => {
	if (!PriceSpanExists) {
		document.querySelector(".price>p").append(priceSpan);
		PriceSpanExists = true;
	}
	totalPrice = sizePrice + saucesPrice + toppingsPrice;
	priceSpan.innerText = totalPrice;

}
showTotalPrice();

document.querySelector("#pizzasize").addEventListener("click", (e) => {
	if (e.target.id === "small") {
		sizePrice = 150;
	} else if (e.target.id === "mid") {
		sizePrice = 200;
	} else {
		sizePrice = 250;
	}
	showTotalPrice();
})


// dragstart - вызывается в самом начале переноса перетаскиваемого элемента.
// dragend - вызывается в конце события перетаскивания - как успешного, так и отмененного.
// dragenter - происходит в момент когда перетаскиваемый объект попадает в область целевого элемента.
// dragleave - происходит когда перетаскиваемый элемент покидает область целевого элемента.
// dragover - происходит когда перетаскиваемый элемент находиться над целевым элементом.
// drop - вызывается, когда событие перетаскивания завершается отпусканием элемента над целевым элементом.
const [...sources] = document.querySelectorAll(".draggable");

sources.forEach(element => {
	let source = element;
	// начало операции drag
	source.addEventListener('dragstart', function (evt) {
		this.style.border = "3px dotted #000"; // меняем стиль в начале операции drag & drop

		// Свойство effectAllowed управляет визуальным эффектом (чаще всего это вид указателя мыши), который браузер создает в ответ 
		// на тип происходящего  события перетаскивания (перемещение, копирование и т. п.).
		// http://msdn.microsoft.com/en-us/library/ie/ms533743%28v=vs.85%29.aspx
		evt.dataTransfer.effectAllowed = "move";

		// Метод setData(...) сообщает механизму перетаскивания в браузере, какие данные из перетаскиваемого объекта должен «поймать»
		// целевой элемент, также называемый зоной приема. Здесь мы указываем, что передаваемые данные это id элемента
		evt.dataTransfer.setData("Text", this.id);

		
	}, false);

	// конец операции drag
	source.addEventListener("dragend", function (evt) {
		this.style.border = ""; // удаляем стили добавленные в начале операции drag & drop 


	}, false);



});
var target = document.getElementById("target");

// перетаскиваемый объект попадает в область целевого элемента
target.addEventListener("dragenter", function (evt) {
	this.style.border = "3px solid red";
}, false);

// перетаскиваемый элемент покидает область целевого элемента
target.addEventListener("dragleave", function (evt) {
	this.style.border = "";
}, false);

target.addEventListener("dragover", function (evt) {
	// отменяем стандартное обработчик события dragover.
	// реализация данного обработчика по умолчанию не позволяет перетаскивать данные на целевой элемент, так как большая
	// часть веб страницы не может принимать данные.
	// Для того что бы элемент мог принять перетаскиваемые данные необходимо отменить стандартный обработчик.
	if (evt.preventDefault) evt.preventDefault();
	return false;
}, false);

// перетаскиваемый элемент отпущен над целевым элементом
let secondSouce = false;
let secondTopping = false;

target.addEventListener("drop", function (evt) {

	// прекращаем дальнейшее распространение события по дереву DOM и отменяем возможный стандартный обработчик установленный браузером.
	if (evt.preventDefault) evt.preventDefault();
	if (evt.stopPropagation) evt.stopPropagation();

	this.style.border = "";
	var id = evt.dataTransfer.getData("Text"); // получаем информации которая передавалась через операцию drag & drop 

	var elem = document.getElementById(id);
	let elemName = document.getElementById(id).nextElementSibling.innerText;

	// добавляем элемент в целевой элемент. Так как в DOM не может быть два идентичных элемента - элемент удаляется со своей старой позиции.
	if (/sauce/.test(id)) {
		if (secondSouce) {
			document.querySelector(".sauces>p").append(", " + elemName);
		} else {
			document.querySelector(".sauces>p").append(elemName);
			secondSouce = true;

		}
		saucesPrice += 15;
		showTotalPrice();

	} else {
		if (secondTopping) {
			document.querySelector(".topings>p").append(", " + elemName);
		} else {
			document.querySelector(".topings>p").append(elemName);
			secondTopping = true;

		}
		toppingsPrice += 20;
		showTotalPrice();

	}
	this.appendChild(elem);

	spans.forEach(element => {
		if (element.innerText == elem.getAttribute("alt")) {
			let elemBack = document.createElement("img");

			elemBack.setAttribute("src", `${elem.getAttribute("src")}`)
			elemBack.style.opacity = "20%";
			let xpath = `//span[text()='${elem.getAttribute("alt")}']`;
			let matchingElement = document.evaluate(xpath, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
			//let tempIngredObject = elemBack;
			console.log(matchingElement, elemBack);
			matchingElement.before(elemBack);

		}
	});



	return false;
}, false);
document.querySelector("#target").addEventListener("click", (e) => {
	if (e.target.className == "draggable") {
		if (/sauce/.test(e.target.id)) {
			saucesPrice -= 15;
			let saucesString = document.querySelector(".sauces>p").innerHTML;

			if (saucesString.includes(`, ${e.target.getAttribute("alt")}`)) {
				saucesString = saucesString.replaceAll(`, ${e.target.getAttribute("alt")}`, "");
				document.querySelector(".sauces>p").innerHTML = saucesString;
			} else {
				saucesString = saucesString.replaceAll(`${e.target.getAttribute("alt")}`, "");
				document.querySelector(".sauces>p").innerHTML = saucesString;
			}
			if (document.querySelector(".sauces>p").innerHTML == "Соуси:") {
				secondSouce = false;
			}
			showTotalPrice();
		} else {
			toppingsPrice -= 20;
			let toppingsString = document.querySelector(".topings>p").innerHTML;

			if (toppingsString.includes(`, ${e.target.getAttribute("alt")}`)) {
				toppingsString = toppingsString.replaceAll(`, ${e.target.getAttribute("alt")}`, "");
				document.querySelector(".topings>p").innerHTML = toppingsString;
			} else {
				toppingsString = toppingsString.replaceAll(`${e.target.getAttribute("alt")}`, "");
				document.querySelector(".topings>p").innerHTML = toppingsString;
			}
			if (document.querySelector(".topings>p").innerHTML == "Топiнги:") {
				secondTopping = false;
			}
			showTotalPrice();

		}
		spans.forEach(element => {
			if (element.innerText == e.target.getAttribute("alt")) {
				let xpath = `//span[text()='${e.target.getAttribute("alt")}']`;
				let matchingElement = document.evaluate(xpath, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
				let tempIngredObject = e.target;

				element.previousElementSibling.remove();

				matchingElement.before(tempIngredObject);
				

			}
		});
	}
});

//Валідація форми 
const validate = (target) => {
	switch (target.getAttribute("name")) {
		case "name": return /^[a-я ,.'-]+$/i.test(target.value);
		case "phone": return /^\+380\d{9}$/.test(target.value);
		case "email": return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(target.value);
	}
};

let [...orderFormInputs] = document.querySelectorAll("input");
orderFormInputs = orderFormInputs.filter((element) => {
	return element.className != "button" && element.className != "radioIn";
})

orderFormInputs.forEach((element) => {
	element.addEventListener("change", (event) => {

		if (!validate(event.target)) {
			event.target.classList.add("error");
			event.target.classList.remove("success");
		} else {
			event.target.classList.remove("error");
			event.target.classList.add("success");

		}
	})
});
let resetFormBtn = document.querySelector("[type=reset]");
let submitOrderBtn = document.querySelector("[type=button]");

resetFormBtn.addEventListener("click", () => {
	orderFormInputs.forEach((element) => {
		element.value = "";
	})
});

submitOrderBtn.addEventListener("click", () => {
	let validationResults = orderFormInputs.map((element) => {
		return validate(element);
	});
	if (!validationResults.includes(false)) {
		document.location = "./thank-you.htm"
	};
});

let movingBanner = document.querySelector('#banner');
movingBanner.addEventListener("mouseover", () => {
	movingBanner.style.left = `${Math.random() * 1720}px`;
	movingBanner.style.top = `${Math.random() * 680}px`;
	console.log(movingBanner.style.top);
	console.log(movingBanner.style.left);
});
